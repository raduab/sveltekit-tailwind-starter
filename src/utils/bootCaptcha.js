import { loader } from '@utils/dynamicLoader'

export const bootCaptcha = () =>
  new Promise((resolve, reject) => {
    try {
      loader({
        urls: [
          {
            type: 'script',
            src: '//www.google.com/recaptcha/api.js',
          },
        ],
        testCondition: () => !!window.grecaptcha,
        callback: () => resolve(true),
      })
    } catch (e) {
      return reject(false)
    }
  })
