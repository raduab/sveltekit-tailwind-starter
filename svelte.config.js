import preprocess from 'svelte-preprocess'
import adapter from '@sveltejs/adapter-static'
import path from 'path'
import precompileIntl from 'svelte-intl-precompile/sveltekit-plugin.js'

const mode = process.env.NODE_ENV
const dev = mode === 'development'

const config = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: [
    preprocess({
      sourceMap: dev,
      postcss: true,
      preserve: ['ld+json'],
      babel: {
        presets: [
          [
            '@babel/preset-env',
            {
              loose: true,
              // No need for babel to resolve modules
              modules: false,
              targets: {
                // ! Very important. Target es6+
                esmodules: true,
              },
            },
          ],
        ],
      },
    }),
  ],

  kit: {
    // By default, `npm run build` will create a standard Node app.
    // You can create optimized builds for different platforms by
    // specifying a different adapter
    adapter: adapter(),

    // Whether to remove, append, or ignore trailing slashes when resolving URLs to routes.
    trailingSlash: 'always',

    vite: {
      resolve: {
        alias: {
          // these are the aliases and paths available throughout the app
          '@components': path.resolve('./src/components'),
          '@utils': path.resolve('./src/utils'),
        },
        plugins: [
          precompileIntl('resources/lang'), // path to locales definition
        ],
      },
    },
  },
}

export default config
