const axios = require('axios')

const { RECAPTCHA_SECRET_KEY, VITE_APP_URL } = process.env

exports.handler = async event => {
    try {
        if (event['httpMethod'] !== 'POST') {
            throw {
                ErrorCode: 1,
                Message: `Unexpected HTTP method "${event['httpMethod']}"`,
            }
        }
        const token = JSON.parse(event.body)

        const recaptchaVerifyResponse = await axios.get(
            `https://www.google.com/recaptcha/api/siteverify?secret=${RECAPTCHA_SECRET_KEY}&response=${token.recaptchaToken}&remoteip=${VITE_APP_URL}`,
            {
                method: 'POST',
            },
        )

        if (!recaptchaVerifyResponse.data.success) {
            return {
                statusCode: 400,
                headers: { 'content-type': 'application/json; charset=utf-8' },
                body: JSON.stringify(recaptchaVerifyResponse.data),
            }
        }

        return {
            statusCode: 200,
            headers: { 'content-type': 'application/json; charset=utf-8' },
            body: JSON.stringify(recaptchaVerifyResponse.data),
        }
    } catch (error) {
        return {
            statusCode: 500,
            headers: { 'content-type': 'application/json; charset=utf-8' },
            body:
                JSON.stringify(error) === JSON.stringify({})
                    ? JSON.stringify({
                          ErrorCode: 0,
                          Message: error.toString(),
                      })
                    : JSON.stringify(error),
        }
    }
}
