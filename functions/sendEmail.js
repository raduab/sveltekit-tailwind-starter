const { MAILGUN_API_KEY, MAILGUN_DOMAIN, MAILGUN_URL, MAILGUN_SENDER } =
  process.env
const mailgun = require('mailgun-js')({
  apiKey: MAILGUN_API_KEY,
  domain: MAILGUN_DOMAIN,
  url: MAILGUN_URL,
})

exports.handler = async event => {
  if (event.httpMethod !== 'POST') {
    return {
      statusCode: 405,
      body: 'Method Not Allowed',
      headers: { Allow: 'POST' },
    }
  }

  const data = JSON.parse(event.body)
  const {
    fName,
    lName,
    phone,
    email,
    faultOption,
    brokerName,
    location,
    idPlate,
    sourceSelect,
    privacyCheckbox,
  } = data
  console.log(data)
  if (
    !fName ||
    !lName ||
    !phone ||
    !email ||
    !faultOption ||
    !brokerName ||
    !location ||
    !idPlate
  ) {
    return {
      statusCode: 422,
      body: 'Missing required fields.',
    }
  }

  const mailgunData = {
    from: `${fName} ${lName} <${data.email}>`,
    to: MAILGUN_SENDER,
    'h:Reply-To': data.email,
    subject: `Cerere masina de inlocuire de la ${fName} ${lName}`,
    template: 'request_a_car',
    'v:name': `${fName} ${lName}`,
    'v:phone': phone,
    'v:email': email,
    'v:faultOption': faultOption,
    'v:brokerName': brokerName,
    'v:location': location,
    'v:idPlate': idPlate,
    'v:sourceSelect': sourceSelect ? sourceSelect : 'no selection',
    'v:privacyCheckbox': privacyCheckbox,
  }

  return mailgun
    .messages()
    .send(mailgunData)
    .then(response => {
      console.log(response)
      return {
        statusCode: 200,
        body: 'Your message was sent successfully! We\'ll be in touch.',
      }
    })
    .catch(error => ({
      statusCode: 422,
      body: error,
    }))
}
